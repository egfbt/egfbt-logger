const c_debug =	console.debug;
const c_info = console.info;
const c_log = console.log;
const c_warn = console.warn;
const c_error = console.error;

const CONSTANTS = require('./constants');
const UTIL = require('util');
const FACTORY = require('./factory');

//CALL THE DEFAULT LOGGERS.....
exports._log = (level, ...args) =>{
	switch(level){
		case CONSTANTS.DEBUG: c_debug(level); break;
		case CONSTANTS.INFO: c_info(level); break;
		case CONSTANTS.WARN: c_warn(level); break;
		case CONSTANTS.ERROR: c_error(level); break;
		default:
			c_log(`* ${level} *:`, ...args);		
	}
}

exports.critical = (message) => {
	try{
		c_log(ac.white.bgRed('CRITICAL ERROR: ') + ' ' + ac.red(UTIL.format(message))); 
	}
	catch(e){
		c_error('LOGGER CRITICAL ERROR', message);
	}
}

//TODO Replace UTIL.format with Deep logger capabilities.  How to check for reused elements....  Need to keep address array?  Pass in recursion. Depth should be a variable..
//May want to look at other loggers to see what they do.
exports.format = UTIL.format;

exports.interceptOn = () => {
	const logger = FACTORY.getLogger('CONSOLE');
	console.debug = logger.debug;
	console.info = logger.info;
	console.warn = logger.warn;
	console.error = logger.error;
}

exports.interceptOff = () => {
	console.debug = c_debug;
	console.info = c_info;
	console.warn = c_warn;
	console.error = c_error;
}
