const Logger = require('./Logger');
const CONSTANTS = require('./constants');
const LogFilter = require('./LogFilter');
const LogHandler = require('./LogHandler');


const def = {
	application: 'unknown',
	handlers: [],
	filters: []
}

const _loggers = Map;
_loggers['ROOT'] = new Logger({domain:null, handlers: def.handlers, filters: def.filters})



//exports.loggers = _loggers;  -- WHY EXPORT THIS?  

/**
 * 
 * @param {mixed} config	1) string = {domain: string}
 */
exports.getLogger = (v) => {
	//Should do better at typechecking here.  Should actually refactor to take a name or config, possibly an array of loggers... Clean up loggers for instance loggers?
	//Maybe use facade pattern to reduce complexity and memory stamp...  this would make hanlders and finters to be log dependant more complex or not reasonable.
	var config = 
		(typeof v == 'undefined') ? {config:'ROOT'}:
		(typeof v == 'string') ? {config:v}: 
		v; 
	const domain = (config.domain == undefined)? 'ROOT':config.domain;
	if(_loggers[domain] == undefined){
		const config = {domain}, def;
		_loggers[domain] = new Logger(config);
	}
	return _loggers[domain];
}

exports.setApplication = (name) => {
	def.application = name;
}

exports.addFilter = (filter) => {
	if(!filter instanceof LogFilter){
		throw new Error('filter must extend LogFilter');
	}
	def.filters.push(filter);
}

exports.addHandler = (handler) => {
	if(!handler instanceof LogHandler){
		throw new Error('Handler needs to extend LogHandler');
	}
	def.handlers.push(handler);

}

