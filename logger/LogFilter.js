module.exports = class LogFilter{


    process(next, logEntry){
        //TODO preliminary checks if any.
        try{
            var response = this.filter(logEntry);
            next(response);
        }
        catch(e){
            e.logEntry = logEntry;
            next(e);
        }
    }

    /**
     * 
     * @param {*} next 
     * @param {*} logEntry 
     */
    filter(next, logEntry){ 
        logEntry.args.unshift('Filter not configured properly, it needs to add function filter(next, logEntry) in.  See docs');
        next(next, logEntry) 
    }
}