const logSystem = require('./logSystem');
const lvl = require('./constants');
const LogEntry = require('./LogEntry');

module.exports = class Logger{

	/**
	 * 
	 * @param {string} domain 
	 * @param {array of Filter} filters 
	 * @param {array of Handler} handlers 
	 */
	constructor(domain, filters, handlers){
		this.domain = domain;
		this.filters = filters;
		this.handlers = handlers;
	}

	runFilter(index, logEntry){
		if(index < this.filters.length){
			if(!this.filters[index] instanceof LogFilter) return Error({message:'Expected instance of LogFIlter', actual: this.filters[index]});
			this.filters[index].process.call(null, (index, p) => {this.runFilter(index+1, p)}, logEntry);
		}
		else return logEntry;
	}
	
	_log (level, ...args) {
		const logEntry = new LogEntry(this.domain, level, args);
		const filtered = this.runFilter(0, logEntry);
		if(filtered instanceof Error){
			logSystem.critical('Error filtering message',{initial: logEntry, error: e});
		}
		var found = false;
		for(var i = 0; i < this.handlers.length; i++){
			if(!this.handlers[i] instanceof LogFilter) {
				logSystem.critical('Expected instance of LogHandler, got: ', this.handlers[i]);
			}
			if(this.handlers[i].enabled){
				var results = this.handlers[i].log.call(null, filtered);
				if(results instanceof Error){
					logSystem.critical('Error filtering message',{initial: logEntry, error: results});
				}
				found = true;
			}
		}
		if(!found){
			if(logSystem.intercept){
				logSystem.critical(`Error: No handlers for logger ${this.domain} turning intercept off`);
				logSystem.interceptOff();
			}
			logSystem._log(level, ...args);
		}
	}

	/**
	 * 
	 * @param  {...any} args 
	 */
	debug(...args){
		this._log(lvl.DEBUG, ...args);
	} 
	info(...args){
		this._log(lvl.INFO, ...args);
	} 
	log(...args){
		this._log(lvl.LOG, ...args);
	} 
	warn(...args){
		this._log(lvl.WARN, ...args);
	} 
	error(...args){
		this._log(lvl.ERROR, ...args);
	} 

	/**
	 * Need to work on this...
	 * @param {*} msg 
	 * @param {*} level 
	 */
	box(msg, level = lvl.LOG){
		//TODO rework this out.
		var pad = "                                                                            ";
		this._log(level,
			"\n============================================================================" + "\n"
			+ "=" + pad.substr(0, Math.round(pad.length / 2 - msg.length / 2)) + msg + "=\n"
			+ "============================================================================" + "\n"
			);
	}
}


