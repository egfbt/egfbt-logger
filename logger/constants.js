
exports.OFF = 0;
exports.DEBUG = 1;
exports.INFO = 3;
exports.WARN = 7;
exports.ERROR = 9;
exports.TOP = 100;

const LOG_LEVELS = new Map;
LOG_LEVELS['OFF'] = 0;
LOG_LEVELS['DEBUG'] = 1;
LOG_LEVELS['INFO'] = 3;
LOG_LEVELS['WARN'] = 7;
LOG_LEVELS['ERROR'] = 9;

const LEVELSTR = new Map;
LEVELSTR[0] = 'OFF';
LEVELSTR[1] = 'DEBUG';
LEVELSTR[3] = 'INFO';
LEVELSTR[7] = 'WARN';
LEVELSTR[9] = 'ERROR';


exports.LOG_LEVELS = LOG_LEVELS;
exports.LEVEL_STR = LEVELSTR;