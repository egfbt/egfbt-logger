const CONSTANTS = require('./constants');

class LogHandler{

    constructor(config){
        this.enabled = true;

        switch(typeof config.level){
            case 'undefined': this.level = CONSTANTS.INFO; break;
            case 'string': this.level = getLevelStr(config.level); break;
            case 'number': this.level = config.level; break;
            default: throw new Error('Unexpected configuration for level: ', config.level);            
        }
        if(this.level == 0) this.level = CONSTANTS.TOP; 
    }

    /**
     * 
     * @param {string} value 
     */
    getLevelStr(value){
        const v = value.toUpperCase();
        const r = CONSTANTS.LOG_LEVELS[v];
        if(r == undefined) throw new Error(`${v} is not a recognized log level`);
        return r;
    }

     /**
     * 
     * @param {*} logEntry 
     */
    log(logEntry){
        if(this.enabled === false) return {logEntry, error: new Error('Log handler called after being disabled')};

        //Check Level
        if(logEntry.level < this.level){
            return false;
        }

        try{
            process(logEntry);
            return true;
        }
        catch(e){
            this.enabled = false;
            return {logEntry, error:e};
        }
    }

    process(logEntry){
        console.trace('*** WARNING: LogHandler process not set, please create a function to handle process calls and bind it to "this" ***');
        logEntry.args.unshift('test');
        next(logEntry);
    }

}

module.exports = LogHandler;
