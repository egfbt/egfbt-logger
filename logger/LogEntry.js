const CONSTANTS = require('./constants');



/**
 * Log Entry is an internal structure used to pass data around.  
 */
module.exports = class LogEntry {
	constructor(meta, level, args){
		this.meta = meta;
		this.level = level;
		this.time = new Date();
		this.args = args;
	}

	getShortName(){
		return (this.meta.domain == '_')? 'ROOT': this.meta.domain;
	}

	getName(){
		return this.getShortName();
	}

	getFullName(){
		return `${this.meta.application}.${this.getShortName()}`;
	}

	getLevelText(){
		return CONSTANTS.LEVELSTR[this.level];
	}

}
