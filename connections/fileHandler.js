const FS = require('fs');
const UTIL = require('util');
const DATEFNS = require('date-fns');
const PATH = require('path');
const CONSTANTS = require('./constants');

var _fail;
var cDay = null;
var logFile;
var cHour = null;

exports.init = async (fail) => {
	_fail = fail;
}

const rotateLogs = (ts) => {
	if(logFile){
		logFile.close();
	}
	var max = (systemConfig.logger.file.maxFiles)? systemConfig.logger.file.maxFiles*1:0;
	for(var i = max;i > 0;i--){
		var nFileName = PATH.join(systemConfig.logger.file.path, systemConfig.app.name + '.log.' + i);
		var oFileName = (i == 1) ? 
			PATH.join(systemConfig.logger.file.path, systemConfig.app.name + '.log'):
			PATH.join(systemConfig.logger.file.path, systemConfig.app.name + '.log.' + (i-1));
		
		if(FS.existsSync(nFileName)){ //Delete old file
			FS.unlinkSync(nFileName);
		}
		if(FS.existsSync(oFileName)){ //Move file
			FS.renameSync(oFileName,nFileName);
		}
	}
	logFile = FS.createWriteStream(PATH.join(systemConfig.logger.file.path, systemConfig.app.name + '.log'), {flags:'a'});
	logFile.on('error', function(err){
		_fail(null, err);
	});
}

exports.log = (entry) =>{
	try{
		var d = DATEFNS.format(entry.time,'YYYYMMDD') *1;
		if(d != cDay){
			rotateLogs(d);
			cDay = d;
		}
		writeLog(entry);
	}
	catch(err){
		_fail(entry, err);
	}
}

const writeLog = (entry) => {
	//log file.
	var h = DATEFNS.format(entry.time,'YYYYMMDDH') *1;
	if(h != cHour){
		cHour = h;
		logFile.write(`Date: ${DATEFNS.format(entry.time,'YYYY-MM-DD')}\n`);
	}
	var header =  entry.getHeader();
	logFile.write(`${header} ${entry.getMessage()} \n`,'UTF8');
}
