const nodeoutlook = require('nodejs-nodemailer-outlook');
const IP = require('ip');
const UTIL = require('util');
const DATEFNS = require('date-fns');
const CONSTANTS = require('./constants');

var logs = [];

exports.critical = (message) =>{
	try{
		sendEmail(`Critical Error (${IP.address()})`, message);
	}
	catch(err){}
}

const freshenLogs = () => {
	var aged = new Date().getTime() - 30*1000;
	while(logs.length > 0 && logs[0].time.getTime() < aged) logs.shift();
}

exports.log = async (entry) =>{
	freshenLogs();

	logs.push(entry);
	if(entry.level >= systemConfig.logger.email.trigger){
		await sendErrorMail(entry);

	}
}

var _fail;

exports.init = async (fail) => {
	_fail = fail;
}


const sendErrorMail = async (entry) => {
	var subject = `${CONSTANTS.LOGSTR[entry.level]} detected in ${entry.application}(${entry.ipAddress}).${entry.domain}`;
	var msg = UTIL.format(...entry.args).replace(/(\n)\s*at/g,'<br/> &nbsp; &nbsp; at').replace(/(\n)/g,'<br/>').replace('  ', ' &nbsp;').replace('\t', ' &nbsp; &nbsp;');
	console.trace(msg);
	var body = `
		<h2>Error</h2>
		<h3>Date: ${DATEFNS.format(entry.time, 'DD/MM/YYYY')} <br/>
		Time: ${DATEFNS.format(entry.time, CONSTANTS.timeFormat)} <br/>
		${msg}</h3>
		<hr>
		<h2>History</h2>
	`;
	for(var i = 0; i < logs.length; i++){
		var le = logs[i];
		var msg = UTIL.format(...le.args).replace(/(\n)\s*at/g,'<br/> &nbsp; &nbsp; at').replace(/(\n)/g,'<br/>').replace('  ', ' &nbsp;').replace('\t', ' &nbsp; &nbsp;');

		body += `
		<b>${DATEFNS.format(le.time, CONSTANTS.timeFormat)} ${le.domain}-${CONSTANTS.LOGSTR[le.level]}:</b> ${msg}<br/>
		`;
	}
	await sendEmail(subject, body, entry.email);
}

const sendEmail = async (subject, body, to = null) => {
	try{
		if(to == null) to = systemConfig.logger.email.target;

		await nodeoutlook.sendEmail({
			auth: {
				user: systemConfig.logger.email.user,
				pass: systemConfig.logger.email.pass,
			},
			to,
			subject: subject,
			html: body,
			onError: (e) => {
				_fail(null, `Email failure: ${e} <br /> \n ${subject}  <br/>\n ${body} <br />`);
			},
		

		});
	}
	catch(err) {
		_fail(entry, err);
	}
};