const UTIL = require('util');

var db;
var _application = null;

var queue = [];
var conFail = false;
var _fail;
var _failed = false;
var _schema = '';


const getApplication = async () => {
	if(_application) return _application;
	var res = await db.query(`
		SELECT id FROM ${_schema}application 
		WHERE server = '${systemConfig.app.ipAddress}' 
			AND application = '${systemConfig.app.name}' 
			AND environment = '${systemConfig.environment.name}'
		`);
	if(res.length  > 0){
		_application = res[0].id;
		return res[0].id;
	}
	await db.query(`INSERT INTO ${_schema}application (server, application, environment) 
		VALUES ('${systemConfig.app.ipAddress}', '${systemConfig.app.name}', '${systemConfig.environment.name}')`);

	var res = await db.query(`
		SELECT id FROM ${_schema}application 
		WHERE server = '${systemConfig.app.ipAddress}' 
			AND application = '${systemConfig.app.name}' 
			AND environment = '${systemConfig.environment.name}'
		`);
	if(res.length  > 0){
		_application = res[0].id;
		return res[0].id;
	}
	throw new Error('Could not configure application id.')
}

/**
 * 
 * @param {callback for failed log posting} fail 
 */
exports.init = async( fail ) => {
	try{
		if(!systemConfig.logger.database) return;
		_fail = fail;
		_schema = systemConfig.logger.database.schema;
		db = await CORE.getPool('loggerDatabase', systemConfig.logger.database.connection);
		application = await getApplication();
		handler();
	}
	catch(err){
		fail(null, err);
	}
}

var domains = new Map();

const getDomain = async (domain) =>{
	domain = domain.replace('\'','');
	if(domains.has(domain)) return domains.get(domain);
	var params = [application, domain];
	var res = await db.query(`
		SELECT id FROM ${_schema}logger WHERE application = '${application}' AND subsystem = '${domain}'`);
	var rdom;
	if(res.length == 1){
		rdom = res[0].id;
	}
	else{
		var res = await db.query(`INSERT INTO ${_schema}logger (application, subsystem) VALUES ('${application}', '${domain}')`);

		var res = await db.query(`SELECT id FROM ${_schema}logger WHERE application = '${application}' AND subsystem = '${domain}'`);

		if(res.length  > 0){
			rdom = res[0].id;
		}
		else{
			throw new Error('Could not configure application id.')
		}
	}
	domains.set(domain, rdom);
	return rdom;
}

const processItem = async(entry) => {
	try{
		var domain = await getDomain(entry.domain);
		var args = UTIL.format(...entry.args).replace(/'/g,`''`).replace(/\n/g,' ');
		const qry = `INSERT INTO ${_schema}entry (logger, level, details) VALUES (${domain}, ${entry.level}, '${args}')`;		
		await db.query(qry);
	}
	catch(err){
		conFail = true;
		_fail(entry, err);
	}

}

const handleQueue = async() => {
	while(queue.length > 0){
		var entry = queue.shift();
		await processItem(entry);
	}
}

const handler = async () => {
	while(!db && !_failed) CORE.sleep(100);

	while(true){
		await handleQueue();
		await CORE.sleep(10);
	}
}


const addToQueue = (logEntry) => {
	queue.push(logEntry);
}


exports.log = ( logEntry ) => {
	if(!conFail){
		addToQueue(logEntry);
	}
	else{
		_fail(logEntry);
	}
}

exports.logSystemStatus = async (system, status) => {
	await db.query(`INSERT INTO ${_schema}status (application, subsystem, status) VALUES ('${_application}','${system}','${status}')`);
}