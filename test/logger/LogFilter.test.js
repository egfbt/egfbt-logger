const LogFilter = require('../../logger/LogFilter');
const assert = require('assert');

class MyFilter extends LogFilter{
    filter(next, logEntry){
        logEntry.args.unshift('test');
        next(logEntry);
    }
}

//setSetting, getSetting, resetConfig, loadConfig, *config
describe('Log Filter', () => {
    it('General Test', () =>{
        var logEntry = {
            args: [1,2,3]
        }

        const filter = new MyFilter();

        logEntry = filter.fo(logEntry)

        console.trace(logEntry);

    });
});




// describe('Ensure it loaded config files as expected', () =>{
// 	CORE.configure('Test', './test/environment');
// 	it('should have loaded logger.js config', () =>{
// 		assert.equal(systemConfig.logger.core, 'debug');
// 	});
// });

// describe('Ensure it loaded config files as expected', () =>{
// 	it('should have loaded config without logger and err', () =>{
// 		CORE.configure('Test', './environment/');
// 		assert.equal(systemConfig.logger, undefined);
// 	});
// });

