const constants = require('./logger/constants');
const factory = require('./logger/factory');


const defaultLogger = factory.getLogger();


const system = require('./logger/system');

module.exports = {
	...constants,
	...factory
}


// var _loggers = {
// 	_: new Logger('_')
// };


// exports.interceptOn = (catcher = _loggers._) =>{
// 	console.debug = (...args) =>{catcher.debug(...args)};
// 	console.info = (...args) =>{catcher.info(...args)};
// 	console.log = (...args) =>{catcher.log(...args)};
// 	console.warn = (...args) =>{catcher.warn(...args)};
// 	console.error = (...args) =>{catcher.error(...args)};
// 	console.trace = (...args) =>{CONSTANTS.c_log(...args)};
// }

// exports.interceptOff = () =>{
// 	console.debug = CONSTANTS.c_debug;
// 	console.info = CONSTANTS.c_info;
// 	console.log = CONSTANTS.c_log;
// 	console.warn = CONSTANTS.c_warn;
// 	console.error = CONSTANTS.c_error;
// 	console.trace = CONSTANTS.c_trace;
// }

// /**
//  * filter: (next, level, ...args) => { .... ; return or next(level, ...args);}
//  */
// exports.addFilter = (filter) => {
// 	_loggers._.addFilter(filter);
// }

// exports.debug = (...args) =>{
// 	_loggers._.debug(...args);
// }

// exports.info = (...args) =>{
// 	_loggers._.info(...args);
// }

// exports.log = (...args) =>{
// 	_loggers._.log(...args);
// }

// exports.warn = (...args) =>{
// 	_loggers._.warn(...args);
// }

// exports.error = (...args) =>{
// 	_loggers._.error(...args);
// }

// exports.outputHeader = (...args) =>{
// 	_loggers._.outputHeader(msg);
// }