// var logger = require('../index');

// logger.setApplication('test app');

// console.trace('\nTurning on intercept');
// logger.interceptOn();
// console.debug('debug - should not show'); 
// console.info('info - shoulw show');
// logger.setConfig({console: 'debug'});
// console.debug('debug - should show');

// console.trace('\nTurning off intercept');
// logger.interceptOff();
// console.debug('debug - should be normal');
// console.trace('\nTurning on intercept');
// logger.interceptOn();

// console.trace('\nUsing default (logger)');
// logger.debug('debug - should show');
// logger.info('info - should show');

// console.trace('\nUsing domained logger');
// const l = logger.getLogger('test');
// l.debug('debug - should show');
// l.info('info - should show');

// console.trace('\nsetting console level to info');
// logger.setConfig({console: 'info'});
// l.debug('test.debug - should not show');
// l.info('test.info - should show');
// logger.debug('logger.debug - should not show');
// logger.info('logger.info - should show');

// console.trace('Setting domain logger to WARN');
// logger.setConfig({test:'warn'});
// l.log('test.log - should not show');
// l.warn('test.warn - should show');
// l.error('test.error - should show');
// logger.log('logger.log - should show');
// logger.warn('logger.warn - should show');
// logger.error('logger.error - should show');

// console.trace('Setting domain logger to OFF');
// logger.setConfig({'test':'OFF'});
// l.debug('debug');
// l.info('info');
// l.log('log');
// l.warn('warn');
// l.error('error');
// console.trace('Clearing domain logger');
// logger.setConfig({'test':null});
// l.debug('test.debug - should not show');
// l.info('test.info - should show');
// logger.debug('logger.debug - should not show');
// logger.info('logger.info - should show');

// console.trace('adding a domain filter');
// l.addFilter((next, ...args) =>{
// 	console.trace('mw')
// 	if(args.includes('SKIP')) return;
// 	next();
// });
// l.log('log', 'SKIP');
// l.log('log', 'dont skip');
// l.log('log', 'dont SKIP');

