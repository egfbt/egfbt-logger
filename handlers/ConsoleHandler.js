const CONSTANTS = require('../logger/constants');

const LogHandler = require('../logger/LogHandler');
const DATEFNS = require('date-fns');

const ac = require('ansi-colors');
const UTIL = require('util');


class ConsoleHandler extends LogHandler{

	constructor(config){
		super(config);
		this.dateFormat = (!config.dateFormat)? 'HH:mm:ss.SS': config.dateFormat;
		//TODO Check that date format is valid.

	}

	getHeader(logEntry){
		return `${DATEFNS.format(logEntry.time, this.dateFormat)} (${logEntry.getShortName()}${(logEntry.pre)?'-pre':''}) ${logEntry.getLevelText()}: `;
	}

	getBody(logEntry){
		return UTIL.format(...this.args);
	}

	/**
	 * 
	 * @param {*} logEntry 
	 */
	process(logEntry){
		const header = this.getHeader(logEntry);
		const body = this.getBody(logEntry);

		switch(entry.level){
			case CONSTANTS.DEBUG: CONSTANTS.c_log(ac.blue(header) + ' ' + ac.white(message)); break;
			case CONSTANTS.INFO:  CONSTANTS.c_log(ac.green(header) + ' ' + ac.white(message)); break;
			case CONSTANTS.WARN:  CONSTANTS.c_log(ac.red(header) + ' ' + ac.white(message)); break;
			case CONSTANTS.ERROR: CONSTANTS.c_log(ac.white.bgRed(header) + ' ' + ac.red(message)); break;		
			default: throw new Error('Unknown Log level', entry.level);
		}
	}
}

module.exports = {

	getConsoleHandler:(config = {level: CONSTANTS.INFO})=>{
		return new ConsoleHandler(config);
	},
	

}


