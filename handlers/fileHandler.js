const CONSTANTS = require('../logger/constants');
const LogHandler = require('../logger/LogHandler');
const DATEFNS = require('date-fns');
const UTIL = require('util');
const FS = require('fs');
const PATH = require('path');
const logSystem = require('../logger/logSystem');




class FileHandler extends LogHandler{

	constructor(config){
		super(config);
        this.dateFormat = (!config.dateFormat)? 'HH:mm:ss.SS': config.dateFormat;
        if(config.path != undefined){
            if(FS.existsSync(config.path)){
                this.path = config.path;
            }
            else{
                throw new Error(`Cannot identify path: ${config.path} `);
            }
        }
        else{
            this.path = '.';
        }
        this.maxFiles = 3;
        if(config.maxFiles != undefined){
            if(typeof config.maxFiles == 'number'){
                this.maxFiles = config.maxFiles;
            }
            else{
                var c = config.maxFiles *1;
                if(c > 0){
                    this.maxFiles = c;
                }
                else throw new Error('Unable to identify numeric value for maxFiles in configuration');
            }
        }
        if(! typeof config.name == 'string') throw new Error('name is required for configuration');
        this.name = config.name;
    }
    
    rotateLogs(ts){
        if(this.logFile){
            this.logFile.close();
        }
        for(var i = this.maxFiles; i > 0; i--){
            var nFileName = PATH.join(this.path, this.name + '.log.' + i);
            var oFileName = (i == 1) ? 
                PATH.join(this, this.name + '.log'):
                PATH.join(this.path, this.name + '.log.' + (i-1));
            
            if(FS.existsSync(nFileName)){ //Delete old file
                FS.unlinkSync(nFileName);
            }
            if(FS.existsSync(oFileName)){ //Move file
                FS.renameSync(oFileName,nFileName);
            }
        }
        logFile = FS.createWriteStream(PATH.join(this.path, this.name + '.log'), {flags:'a'});
        logFile.on('error', function(err){
            logSystem.critical('File Log Error:' + err);
            throw err;
        });
    }


	getHeader(logEntry){
		return `${DATEFNS.format(logEntry.time, this.dateFormat)} (${logEntry.getName}${(logEntry.pre)?'-pre':''}) ${logEntry.getLevelText()}: `;
	}

	getBody(logEntry){
		return UTIL.format(...this.args);
	}

	/**
	 * 
	 * @param {*} logEntry 
	 */
	process(logEntry){
		
        var d = DATEFNS.format(logEntry.time,'YYYYMMDD') *1; //Should make log rotation based on othero ptions besides day only.
        if(d != this.cDay){
            rotateLogs(d);
            this.cDay = d;
        }
        writeLog(logEntry);

        const header = this.getHeader(logEntry);
		const body = this.getBody(logEntry);

        logFile.write(`${header} ${body} \n`,'UTF8');
    }
}

module.exports = {

	getFileHandler:(config = {level: CONSTANTS.INFO})=>{
		return new FileHandler(config);
	},
}


